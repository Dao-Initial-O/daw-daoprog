﻿using System.Collections;
using UnityEngine;

/*
 * Made For Dao
 * 物を振動させるスクリプト（ここではカメラ）
 */

public class CameraShake : MonoBehaviour
{
    //-- 物を震わせたい時、duration（揺らす時間）とmagnitude（揺れの大きさ）を受け取り、コルーチンを呼び出す。 --//
    public void Shake(float duration, float magnitude)
    {
        StartCoroutine(DoShake(duration, magnitude));
    }

    //-- 震わせる処理 --//
    private IEnumerator DoShake(float duration, float magnitude)
    {
        Vector3 pos = new Vector3(0,0,0);
        float time = 0f;       //時間記録用

        //-- durationの方が小さくなるまで下記の処理（ランダムに移動させる処理）を続ける --//
        while (time < duration)
        {
            //-- 上下にランダムに揺らす --//
            float x = pos.x + Random.Range(-1f, 1f) * magnitude;      //ポジションに、-1から1までのランダムな数字とmagnitudeで設定した値を掛けた数値を足す。
            float y = pos.y + Random.Range(-1f, 1f) * magnitude;

            transform.localPosition = new Vector3(x, y, pos.z);     //ポジションを先ほど設定したランダムな位置にする。

            time += Time.deltaTime;      //経過時間を足す。

            yield return null;      //ここで処理を中断。1フレーム待って次の処理（繰り返し又は終了）へ
        }

        transform.localPosition = pos;      //位置を元に戻す。
    }
}