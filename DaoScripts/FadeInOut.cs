﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Made for Dao
 * フェードイン、フェードアウトの処理
 */

public class FadeInOut : MonoBehaviour
{
    //-- Inspectorから設定 --//
    [SerializeField]
    private CameraFader _cameraFader = null;    //カメラフェーダー型のオブジェクトを取得。設定しなければnull。

    //フェードアウト
    public void FadeOut()
    {
        _cameraFader.FadeOut();
    }

    //フェードイン
    public void FadeIn()
    {
        _cameraFader.FadeIn();
    }

}