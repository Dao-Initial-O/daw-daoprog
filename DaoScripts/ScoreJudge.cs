﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoreJudge : MonoBehaviour
{
    //public static int score;

    //public static bool over = false;

    public Text playtext;
    public Text highScoreText;
    //ハイスコアを表示するText

    public int highScore;
    public int score2;
    public int score3;
    //ハイスコア用変数

    private string key = "HIGH SCORE";
    //ハイスコアの保存先キー

    // Use this for initialization
    void Start()
    {
        highScore = PlayerPrefs.GetInt(key, 0);
        //保存しておいたハイスコアをキーで呼び出し取得し保存されていなければ0になる
        highScoreText.text = "HighScore : " + highScore.ToString();
        //ハイスコアを表示
        score3 = GameController.getscore();
    }

    // Update is called once per frame
    void Update()
    {

        //playtext.text = score3.ToString();
        //ハイスコアより現在スコアが高い時
        if (score3 > highScore)
        {

            highScore = score3;
            //ハイスコア更新

            PlayerPrefs.SetInt(key, highScore);
            //ハイスコアを保存

            highScoreText.text = "HighScore : " + score3.ToString();
            //ハイスコアを表示
        }
        score2 = score3;
        playtext.text = "Score : " + score2.ToString();
    }

    /*

    public static int getscore()
    {
        return score;
    }
    */

    /*
    public static bool getover()
    {
        return over;
    }
    */

}