﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//namespace SceneChanger

    //=== シーン遷移処理 ===//
    public class SceneChanger : MonoBehaviour
    {

        public string _scene;       //遷移シーン名
        public string _GameOver;    //遷移シーン名（ゲームオーバー専用）
        public float _fadeDuration; //フェード時間
        public float _fadeDelay;    //フェード間の遅延時間

        //--- シーン遷移処理 ---//
        public void Change()
        {
            SceneNavigator.Instance.ChangeScene(_scene, fadeDuration: _fadeDuration, fadeDelay: _fadeDelay);
        }

        //--- シーン遷移処理（ゲームオーバー専用） ---//
        public void ChangeGameOver()
        {
            SceneNavigator.Instance.ChangeScene(_GameOver, fadeDuration: _fadeDuration, fadeDelay: _fadeDelay);
        }

    }

