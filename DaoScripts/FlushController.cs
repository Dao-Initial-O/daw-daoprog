﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/* 
 * Made for Dao
 * 画面をフラッシュさせる
 * 正しくはUIのイメージの色を変更しているだけ。
 */

public class FlushController : MonoBehaviour
{

    Image img;      //UIのイメージを格納する用

    //-- イメージを取得し、色を透明にする。 --//
    void Start()
    {
        img = GetComponent<Image>();
        img.color = Color.clear;
    }

    //-- コレを呼び出すと画面が赤くなる --//
    public void Flush()
    {
        //イメージの色を赤色にし、透明度を半分にする。
        this.img.color = new Color(0.5f, 0f, 0f, 0.5f);
    }

    void Update()
    {
            this.img.color = Color.Lerp(this.img.color, Color.clear, Time.deltaTime);       //イメージの色を徐々に透明にしていく。
    }

}