﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2019.12.30
 * made for Dao
 * RigidBodyでノーツを飛ばす
 */

public class NotesScript2 : MonoBehaviour
{

    private GameObject PlayerObj;   //プレイヤーの位置情報取得用
    private GameObject GameController;  //ゲームコントローラーコンポーネント取得用

    public string PlayerTag;    //狙う相手（プレイヤー）が持つタグを指定。

    //-- 相対距離計算用 --//
    float distance;
    Vector3 Player_Transform;
    Vector3 Notes_Transform;

    float time; //オフセット時間取得用

    float speed; //速さ

    float OffsetTime = 0.0f; //ノーツのタイミング収録時に遅延が発生した時に足し引き出来る用。

    float StartTime;    //呼び出された時間

    float EndTime;  //ノーツを消す時間

    void Start()
    {
        //-- ゲームコントローラー取得 --//  
        GameController = GameObject.FindGameObjectWithTag("GameController");

        //-- 位置情報取得 --//
        PlayerObj = GameObject.FindGameObjectWithTag(PlayerTag);
        Player_Transform = PlayerObj.transform.position;
        Notes_Transform = this.transform.position;

        //-- ゲームコントローラーからオフセット時間を取得 --//
        time = GameController.GetComponent<GameController>().TimeOffset;
        OffsetTime = GameController.GetComponent<GameController>().NotesOffset;

        //-- 相対距離計算 --//
        distance = (Notes_Transform - Player_Transform).magnitude;

        //-- 速さ算出 --//
        speed = Mathf.Abs(distance) / Mathf.Abs(time)　+ OffsetTime;

        //-- ノーツをオフセットタイムの一秒後に削除するための準備 --//
        StartTime = 0;
        EndTime = 1 + Mathf.Abs(time);
    }

    void Update()
    { 
        Vector3 force;  //ベクトル保管用
        force = this.gameObject.transform.forward * speed;  //このノーツの向きに速度を掛ける。
        Rigidbody rb =  this.gameObject.GetComponent<Rigidbody>();  //リジッドボディ取得。
        rb.velocity = force;    //ベロシティでノーツを動かす。

        //-- このノーツが生成されてからの時間がEndTimeより大きくなったら削除 --//
        StartTime += Time.deltaTime;
        if (EndTime < StartTime) Destroy(this.gameObject) ;
    }

    //-- 何かしらに当たったら消す処理 --//
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "EndNotu")
            Destroy(this.gameObject);
    }
}