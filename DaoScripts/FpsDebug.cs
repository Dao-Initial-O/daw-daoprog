﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/*
 * 2020/1/9
 * Made for Dao
 * fps確認用
 */

public class FpsDebug : MonoBehaviour
{
    private Text FpsText;

    private void Start()
    {
        FpsText = this.GetComponent<Text>();
    }

    void Update()
    {
        if (Time.frameCount % Application.targetFrameRate == 0)
        {
            FpsText.text = "FPS:" + (int)(1 / Time.deltaTime);
        }
    }
}