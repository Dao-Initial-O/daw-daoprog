﻿using UnityEngine;
using System.Collections;

/*
 * Made for Dao
 * SE再生用
 */

public class SoundTest : MonoBehaviour
{
    public AudioClip[] _SE;
    private AudioSource[] _Source;

    /*
     エレメント０HP減少
     エレメント１回避
     エレメント２敵HP現象
     エレメント３弾発射
     エレメント４剣
     エレメント５盾
     エレメント６武器出し
     エレメント７武器決定
     エレメント８武器破壊音
         */

    void Start()
    {
        _Source = this.gameObject.GetComponents<AudioSource>();
        _Source[0].clip = _SE[0];
        _Source[1].clip = _SE[1];
        _Source[2].clip = _SE[2];
        _Source[3].clip = _SE[3];
        _Source[4].clip = _SE[4];
        _Source[5].clip = _SE[5];
        _Source[6].clip = _SE[6];
        _Source[7].clip = _SE[7];
        _Source[8].clip = _SE[8];
    }

    public void HP()
    {
        _Source[0].Play(); 
    }

    public void Dodge()
    {
        _Source[1].Play();
    }

    public void EnemyHP()
    {

        _Source[2].Play();
    }

    public void Bullet()
    {
        _Source[3].Play();
    }

    public void Sword()
    {
        _Source[4].Play();
    }

    public void Shield()
    {
        _Source[5].Play();
    }

    public void Weapon1()
    {
        _Source[6].Play();
    }

    public void Weapon2()
    {
        _Source[7].Play();
    }

    public void Destroy1()
    {
        _Source[8].Play();
    }
}