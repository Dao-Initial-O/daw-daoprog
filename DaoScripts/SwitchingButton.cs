﻿using UnityEngine;
using System.Collections;

/*
 * Made for Dao
 * 選曲画面の切り替え処理
 */

public class SwitchingButton : MonoBehaviour
{
    //-- 各種オブジェクト取得 --//
    public GameObject MusicArea;
    private GameObject button;
    private AudioSource _audio;

    //-- 開始時にオーディオソースを取得 --//
    private void Start()
    {
        _audio = this.GetComponent<AudioSource>();
    }

    //-- クリックされた時の処理 --//
    public void OnClick()
    {
        _audio.Play();      //オーディオソース再生
        button = GameObject.FindGameObjectWithTag("Button");        //Buttonのタグを持つオブジェクトを取得してbuttonへ格納（各選曲画面のオブジェクト）
        button.SetActive(false);        //buttonを非アクティブ化する。（今あった選曲画面を見えなくして無効化）
        MusicArea.SetActive(true);      //MusicAreaに格納したオブジェクトをアクティブ化する。（表示させたい選曲画面を有効化）
    }
}