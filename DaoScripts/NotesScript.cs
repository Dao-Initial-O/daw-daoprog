﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * made for Dao
 * Transform.positionでノーツを飛ばす
 * 2019.12.30にNotesScript2へ移行。
 */

public class NotesScript : MonoBehaviour
{

    private GameObject targetObj;
    private GameObject targetObj2;
    public GameObject explosion;

    Coroutine coroutine;

    float x_Abs;
    float y_Abs;
    float z_Abs;

    public string TargetTag;    //狙う相手（プレイヤー）
    public string BulletTag;    //狙う場所（的）

    float spedparam;
    float speedparam2;
    float speed;

    private float Y;

    void Start()
    {
        targetObj = GameObject.FindGameObjectWithTag(TargetTag);
        targetObj2 = GameObject.FindGameObjectWithTag(BulletTag);
    }

    void Update()
    {
        x_Abs = Mathf.Abs(this.gameObject.transform.position.x - targetObj2.transform.position.x);
        y_Abs = Mathf.Abs(this.gameObject.transform.position.y - targetObj2.transform.position.y);
        z_Abs = Mathf.Abs(this.gameObject.transform.position.z - targetObj2.transform.position.z);
        if (coroutine == null)
        {
            coroutine = StartCoroutine(MoveCoroutine());
        }
    }

    IEnumerator MoveCoroutine() //このオブジェクトと的オブジェクトの相対距離を取得して掛け算することでスピードを決めているよ。
    {
        float spedparam = (x_Abs * x_Abs) + (y_Abs * y_Abs) + (z_Abs * z_Abs);
        float speedparam2 = Mathf.Sqrt(spedparam);
        float speed = speedparam2 * Time.deltaTime;

        while (x_Abs > 0 || y_Abs > 0 || z_Abs > 0)
        {
            yield return new WaitForEndOfFrame();
            this.gameObject.transform.position = Vector3.MoveTowards(this.gameObject.transform.position, targetObj2.transform.position, speed);
        }
        // print("重なった");
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "EndNotu")
            Destroy(this.gameObject);
    }

}

