﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2020/1/9
 * Made for Dao
 * 移動範囲を制限
 */

public class MovingRange : MonoBehaviour
{
    [Header ("移動値の最大、最小値を設定")]
    public float min = -1.5f;     //最小移動値
    public float max = 1.5f;      //最大移動値

    [Header("制限を解除する際、このチェックを外す")]
    public bool Check = true;

    private GameObject Player;

    void Start()
    {
        Player = this.gameObject;
    }

    void Update()
    {
        if (Check) Range();
    }

    void Range()
    {
        //-- Mathf.Clamp(制限する値,最小値,最大値)を用いて制限する --//
        Player.transform.position = new Vector3(Mathf.Clamp(Player.transform.position.x, min, max),
                                                Mathf.Clamp(Player.transform.position.y, -5, 5),
                                                Mathf.Clamp(Player.transform.position.z, min, max));
    }

}