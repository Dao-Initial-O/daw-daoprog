﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * 2020/1/10
 * Made for Dao
 * カウントダウン処理
 */

public class TimerController : MonoBehaviour
{
    public Text timerText;

    private float totalTime = 4;
    int seconds = 4;

    private AudioSource _audio;

    public bool Check = false;

    private float span = 1f;
    private float currentTime = 0f;

    void Start()
    {
        _audio = this.gameObject.GetComponent<AudioSource>();
    }

    void Update()
    {
       if (this.gameObject.GetComponent<GameController>().Touch()) Check = true;

        if (Check)
        {
            CountDown();

            currentTime += Time.deltaTime;

            if (currentTime > span)
            {
                _audio.Play();
                currentTime = 0f;
                timerText.text = seconds.ToString();

            }

        }else
        {
            totalTime = 4;
        }
    }

    void CountDown()
    {
        totalTime -= Time.deltaTime;
        seconds = (int)totalTime;
        //timerText.text = seconds.ToString();
        if (totalTime < 0)
        {
            totalTime = 4;
           // timerText.text = 4.ToString();
        }
    }
}