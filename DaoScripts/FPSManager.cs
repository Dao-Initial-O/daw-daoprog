﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * 2019.12.29 made Dao
 * FPS管理クラス
 */

public class FPSManager : MonoBehaviour
{
    // FPS管理クラス
    void Awake()
    {
        Application.targetFrameRate = 60;    //数字を変えるとその数字のフレームレートになる。
    }
}