﻿using UnityEngine;
using System.Collections;

public class NotesTimingMaker : MonoBehaviour
{

    private AudioSource _audioSource;
    private float _startTime = 0;

    private float _9pushTime = 0;
    private float _9pushTime2 = 0;

    private float _8pushTime = 0;
    private float _8pushTime2 = 0;

    private float _7pushTime = 0;
    private float _7pushTime2 = 0;

    private float _6pushTime = 0;
    private float _6pushTime2 = 0;

    private float _5pushTime = 0;
    private float _5pushTime2 = 0;

    private float _4pushTime = 0;
    private float _4pushTime2 = 0;

    private float _3pushTime = 0;
    private float _3pushTime2 = 0;

    private float _2pushTime = 0;
    private float _2pushTime2 = 0;

    private float _1pushTime = 0;
    private float _1pushTime2 = 0;

    private float _EndTime = 0;
    private float _EndTime2 = 0;

    private CSVWriter _CSVWriter;

    private bool _isPlaying = false;
    public GameObject startButton;

    private float _9PushTime;
    private float _8PushTime;
    private float _7PushTime;
    private float _6PushTime;
    private float _5PushTime;
    private float _4PushTime;
    private float _3PushTime;
    private float _2PushTime;
    private float _1PushTime;


    void Start()
    {
        _audioSource = GameObject.Find("GameMusic").GetComponent<AudioSource>();
        _CSVWriter = GameObject.Find("CSVWriter").GetComponent<CSVWriter>();
    }

    void Update()
    {
        if (_isPlaying)
        {
            DetectKeys();
        }
    }

    public void StartMusic()
    {
        startButton.SetActive(false);
        _audioSource.Play();
        _startTime = Time.time;
        _isPlaying = true;

    }

    void DetectKeys()
    {

        if (Input.GetKeyDown(KeyCode.Keypad9))
        {
            _9pushTime = Time.time;
            _9pushTime2 = 0;
            _9PushTime = GetTiming();
        }

        if (Input.GetKeyUp(KeyCode.Keypad9))
        {
            _9pushTime2 = Time.time - _9pushTime;
            WriteNotesTiming(_9PushTime, 2, _9pushTime2);
        }

        if (Input.GetKeyDown(KeyCode.Keypad8))
        {
            _8pushTime = Time.time;
            _8pushTime2 = 0;
            _8PushTime = GetTiming();
        }

        if (Input.GetKeyUp(KeyCode.Keypad8))
        {
            _8pushTime2 = Time.time - _8pushTime;
            WriteNotesTiming(_8PushTime, 1, _8pushTime2);
        }

        if (Input.GetKeyDown(KeyCode.Keypad7))
        {
            _7pushTime = Time.time;
            _7pushTime2 = 0;
            _7PushTime = GetTiming();
        }

        if (Input.GetKeyUp(KeyCode.Keypad7))
        {
            _7pushTime2 = Time.time - _7pushTime;
            WriteNotesTiming(_7PushTime, 0, _7pushTime2);
        }

        if (Input.GetKeyDown(KeyCode.Keypad6))
        {
            _6pushTime = Time.time;
            _6pushTime2 = 0;
            _6PushTime = GetTiming();
        }

        if (Input.GetKeyUp(KeyCode.Keypad6))
        {
            _6pushTime2 = Time.time - _6pushTime;
            WriteNotesTiming(_6PushTime, 5, _6pushTime2);
        }

        if (Input.GetKeyDown(KeyCode.Keypad5))
        {
            _5pushTime = Time.time;
            _5pushTime2 = 0;
            _5PushTime = GetTiming();
        }

        if (Input.GetKeyUp(KeyCode.Keypad5))
        {
            _5pushTime2 = Time.time - _5pushTime;
            WriteNotesTiming(_5PushTime, 4, _5pushTime2);
        }

        if (Input.GetKeyDown(KeyCode.Keypad4))
        {
            _4pushTime = Time.time;
            _4pushTime2 = 0;
            _4PushTime = GetTiming();
        }

        if (Input.GetKeyUp(KeyCode.Keypad4))
        {
            _4pushTime2 = Time.time - _4pushTime;
            WriteNotesTiming(_4PushTime, 3, _4pushTime2);
        }

        if (Input.GetKeyDown(KeyCode.Keypad3))
        {
            _3pushTime = Time.time;
            _3pushTime2 = 0;
            _3PushTime = GetTiming();
        }

        if (Input.GetKeyUp(KeyCode.Keypad3))
        {
            _3pushTime2 = Time.time - _3pushTime;
            WriteNotesTiming(_3PushTime, 8, _3pushTime2);
        }

        if (Input.GetKeyDown(KeyCode.Keypad2))
        {
            _2pushTime = Time.time;
            _2pushTime2 = 0;
            _2PushTime = GetTiming();
        }

        if (Input.GetKeyUp(KeyCode.Keypad2))
        {
            _2pushTime2 = Time.time - _2pushTime;
            WriteNotesTiming(_2PushTime, 7, _2pushTime2);
        }

        if (Input.GetKeyDown(KeyCode.Keypad1))
        {
            _1pushTime = Time.time;
            _1pushTime2 = 0;
            _1PushTime = GetTiming();
        }

        if (Input.GetKeyUp(KeyCode.Keypad1))
        {
            _1pushTime2 = Time.time - _1pushTime;
            WriteNotesTiming(_1PushTime, 6, _1pushTime2);
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            _EndTime = Time.time - _EndTime;
            WriteNotesTiming(GetTiming(), 10, _EndTime2);
        }
    }

    void WriteNotesTiming(float push, int num, float num2)
    {
        Debug.Log(GetTiming());
        _CSVWriter.WriteCSV(push.ToString() + "," + num.ToString() + "," + num2.ToString());
    }

    float GetTiming()
    {
        return Time.time - _startTime;
    }
}