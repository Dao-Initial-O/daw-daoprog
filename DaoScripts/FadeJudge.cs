﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Made for Dao
 * シーン遷移中は操作を受け付けないようにするクラス
 */

public class FadeJudge : MonoBehaviour
{

    GameObject eventSystem;     //イベントシステムのオブジェクト格納用。
   
    void Start()
    {
        eventSystem = GameObject.Find("EventSystem");       //EventSystemのタグを持つオブジェクトをeventSystemに格納。（イベントシステム）
    }

    void Update()
    {
        
        //-- シーンナビゲーターでシーンが遷移しているか確認。 --//
        if (SceneNavigator.Instance.IsChanging)
        {
            //シーンが遷移している最終ならイベントシステムを無効化する。
            eventSystem.SetActive(false);
        }
        else
        {
            //シーンが遷移していない時ならイベントシステムを有効化する。
            eventSystem.SetActive(true);
        }
        
    }
}