﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;
using UnityEngine.UI;
//using SceneChanger;

/*
 * Made for Dao
 * ゲームの根幹部分
 */

public class GameController : MonoBehaviour
{
    [Header("スコア保存用")]
    public static int score;

    [Header("ノーツのスポーン用")]
    public GameObject Enemy;
    public GameObject[] notes;
    public GameObject[] Longnotes;
    public GameObject _Player;
    //-- CSVから読みだした値の格納用 --//
    private float[] _timing;
    private int[] _lineNum;
    private float[] _NotesTime;
    //-- ノーツのカウント --//
    private int _notesCount = 0;
    //-- CSVファイルの場所 --//
    [Header("CSVファイル")]
    public string filePass;
    
    private AudioSource _audioSource;   //BGM取得
    //public GameObject _SESource;

    private float _startTime = 0;       //ゲーム開始時間

    [Header("timeはノーツの速さ、notesは記録時のズレ、Longは設置間隔")]
    public float timeOffset = 1;      //notesの速さを表す。値が大きいほど遅くなる。
    public float notesOffset = 10f;  //notesのオフセット。記録時に少しずれた時用
    public float _LongNotesInterval = 0.05f;    //ロングノーツのスポーン間隔


    //-- プレイフラグ関連 --//
    private bool _isPlaying = false;
    private bool _EndGame = true;

    //-- プレイヤー周りの表示 --//
    private bool _isPlayHP = false;
    [Header("プレイヤー周辺の表示")]
    public Text scoreText;
    private int _score = 0;
    public Text HP_Text;
    public Slider HP_Slider;
    public int _HP;

    [Header("UI類")]
    public GameObject StartButtn;
    public GameObject StertObject1;
    public GameObject StertObject2;

    [Header("ハイスコア判定オブジェクト")]
    public GameObject _ScoreJudge;      //スコアジャッジ取得用

    [Header("振動パック")]
    public CameraShake shake;   //カメラ振動スクリプト取得用

    [Header("画面を赤くする")]
    public FlushController flush;   //フラッシュコントローラー取得用

    [Header("PCならチェック")]
    public bool PcInput = false;     //PC入力かVR入力か選択する用のブール。

    [Header("チュートリアルならチェック")]
    public bool Tutorial = false;   //チュートリアルの場合trueにする

    [Header("リスタートするならチェック")]
    public bool Restart = false;    //リスタートする場合trueにする

    //-- 外部からオフセットの値を取得、入力させられるように --//
    public float TimeOffset
    {
        get { return this.timeOffset; }  //取得用。
        private set { this.timeOffset = value; } //値入力用
    }

    //-- 外部からオフセットの値を取得、入力させられるように --//
    public float NotesOffset
    {
        get { return this.notesOffset; }  //取得用
        private set { this.notesOffset = value; } //値入力用
    }

    //-- スコアのゲッター。異なるシーンでも呼べる用 --//
    public static int getscore()
    {
        return score;
    }

    //-- 開始時の処理 --//
    void Start()
    {
        if (timeOffset > 0) timeOffset *= -1;   //これがプラスの値だとノーツのタイミングが必ず後にずれてしまうのでマイナスの値に強制変更。

        _audioSource = GameObject.Find("GameMusic").GetComponent<AudioSource>();    //BGMを取得。

        if (Tutorial == false)
        {
            //-- CSVから読み取った値を格納する変数を確保 --//
            _timing = new float[1024];
            _lineNum = new int[1024];
            _NotesTime = new float[1024];

            LoadCSV();  //CSVをロード

            StartCoroutine(WaitCoroutine());    //ボタンが押されるまで処理を止める
        }
    }

    //=== ロングノーツ用コルーチン。num5に場所、num6に秒数が入る ===//
    private IEnumerator LongnotesCoroutine(int notesnum, float notestime) 
    {
        //-- 時間を設定 --//
        float StartTime = Time.time;    //現在の時間用
        float StartTime2 = 0;       //置いた後の時間用
        float StartTime3 = 0;       //経過時間用

        //-- 経過時間がnum6（指定秒数）を超えるまで繰り返す --//
        while (StartTime3 < notestime)
        {
            GameObject targetObj = GameObject.FindGameObjectWithTag(notesnum.ToString());       //ターゲットのオブジェクトをnum5の値に設定してそこへ飛ぶようにする。

            Vector3 target = targetObj.transform.position;      //ターゲットの位置情報を取得。
            Vector3 tmp = Enemy.transform.position;             //ノーツを出す場所の位置情報を取得。

            //-- ノーツのスポーン --//
            GameObject Notes = Instantiate(Longnotes[notesnum],
                new Vector3(tmp.x, tmp.y, tmp.z),
                Quaternion.LookRotation(target - tmp));

            yield return new WaitForSeconds(_LongNotesInterval);        //ロングノーツインターバルの分待つ。

            //-- 経過時間算出 --//
            StartTime2 = Time.time;
            StartTime3 = StartTime2 - StartTime;

        }
    }

    //=== コルーチンでボタンが押されるまで処理を待機 ===//
    
    public IEnumerator WaitCoroutine() 
    {
        Debug.Log("wait...");
        yield return new WaitUntil(Touch);

        //-- 押されたらゲームスタート --//
        Debug.Log("Start!!");
        StartGame();

    }
    

    //=== 入力機器からの操作を受け付け、押されるとゲームが始まる ===//
    public bool Touch()
    {
        if (PcInput)
        {
            //--- debug（PCから何かしら押されたらtrueを返す） ---//
            return Input.anyKey;
        }
        else
        {
            //--- VR操作でbボタンが押されたらtrueを返す ---//
            return OVRInput.GetDown(OVRInput.RawButton.B);
        }
    }

    void Update()
    {
        //-- プレイ中の時、次のノーツを生成 --//
        if (_isPlaying)
        {
            CheckNextNotes();
            scoreText.text = "SC : " + _score.ToString();
          //  scoreText2.text = _score.ToString();
        }

        //--  --//
        if (_isPlayHP)
        {
            HP_Text.text = "HP : "  +_HP.ToString();
            HP_Slider.value = _HP;  //HPバー表示
            //HP_Text2.text = _HP.ToString();
            //Debug.Log("OK");
        }

        //-- 体力が0以下になったらゲームオーバー画面へ --//
        if (_HP < 0　&& Tutorial == false)
        {
            this.gameObject.GetComponent<SceneChanger>().ChangeGameOver();
        }
    }

    //-- ゲームが終了したときの処理 --//
    public void EndGame()
    {
        if (_EndGame)
        {
            _isPlaying = false;
            _isPlayHP = false;
            Debug.Log("End");
            if (Tutorial)
            {
                this.gameObject.GetComponent<TimerController>().Check = false;
                _EndGame = false;
            }
            else if (Tutorial == false)
            {
                score = _score;     //最終スコアを保存
                                    //Debug.Log("End");
                                    //Debug.Log(score);
                Instantiate(_ScoreJudge);       //ハイスコア更新しているかどうか判定
                this.gameObject.GetComponent<SceneChanger>().Change();      //シーン遷移
                _EndGame = false;       //エンドゲームが重複して発動しないようにしておく。
            }

        }
    }

    //-- ゲームがスタートした時の処理 --//
    public void StartGame()
    {
        if (Restart)
        {
            _score = 0;
            _HP = 100;
            //-- CSVから読み取った値を格納する変数を確保 --//
            _timing = new float[1024];
            _lineNum = new int[1024];
            _NotesTime = new float[1024];

            LoadCSV();  //CSVをロード

            StartCoroutine(WaitCoroutine());    //ボタンが押されるまで処理を止める
            Restart = false;
        }

        _startTime = Time.time;     //ゲームが開始した時間を記録。
        _audioSource.Play();

        //-- UIを表示 --//
        _isPlaying = true;
        _isPlayHP = true;

        //-- これもUI? --//
        StartButtn.SetActive(false);
        StertObject1.SetActive(false);
        StertObject2.SetActive(false);
    }

    //-- ノーツを配置 --//
    void CheckNextNotes()
    {
        while (_timing[_notesCount] + timeOffset < GetMusicTime() && _timing[_notesCount] != 0)
        {
            SpawnNotes2(_lineNum[_notesCount], _NotesTime[_notesCount]);
            _notesCount++;
        }
    }

    //-- 的に向かってノーツを生成。 --//
    void SpawnNotes2(int notesnum, float notestime)//
    { 
        //ノーツの番号、num2に秒が入る。

        if (notesnum == 10)
        {
            //numに10が入ってたらゲーム終了
            EndGame();
        }

        if (notestime >= 1.0)
        {
            //num2の値が1以上ならその秒数間だけロングノーツを生成
            StartCoroutine(LongnotesCoroutine(notesnum, notestime));
        }
        else if (notestime != 0)
        {
            //num2が0じゃなかったら普通のノーツを生成
            GameObject targetObj = GameObject.FindGameObjectWithTag(notesnum.ToString());
            Vector3 target = targetObj.transform.position;
            Vector3 tmp = Enemy.transform.position;
            GameObject Notes = Instantiate(notes[notesnum],
                new Vector3(tmp.x, tmp.y, tmp.z),
                Quaternion.LookRotation(target - tmp));
        }

    }

    //-- CSVの読み込み --//
    void LoadCSV()
    {
        int i = 0, j;
        TextAsset csv = Resources.Load(filePass) as TextAsset;
        //Debug.Log(csv.text);
        StringReader reader = new StringReader(csv.text);

        while (reader.Peek() > -1)
        {
            string line = reader.ReadLine();
            string[] values = line.Split(',');
            for (j = 0; j < values.Length; j++)
            {
                _timing[i] = float.Parse(values[0]);
                _lineNum[i] = int.Parse(values[1]);
                _NotesTime[i] = float.Parse(values[2]);
            }
            i++;
        }
    }

    //-- スタートからの経過時間を返す --//
    float GetMusicTime()
    {
        return Time.time - _startTime;
    }

   //スコア足す。
    public void GoodTimingFunc(int num)
    {
        _score = _score +num;
        
    }

    public void HItPointA(int HPA)
    {
        _HP = _HP - HPA;
        shake.Shake(0.25f, 0.25f);
        flush.Flush();

        //効果音再生
        //_SESource.GetComponent<SoundTest>().HP();

    }
}