﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//=== ゲーム終了ボタン ===//
public class QuitButton : MonoBehaviour
{

    private AudioSource _audio;

    public void Quit()
    {
        
        //#if UNITY_EDITOR
        //UnityEditor.EditorApplication.isPlaying = false;
        

        //#elif UNITY_STANDALONE

        UnityEngine.Application.Quit();
        
        //#endif
    }
}